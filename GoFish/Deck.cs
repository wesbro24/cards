﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    public class Deck
    {
        public List<Card> Cards { get; private set; }

        public Deck()
        {
            var suits = Enum.GetValues(typeof(Suit)).Cast<Suit>().ToList();
            var values = Enum.GetValues(typeof(Value)).Cast<Value>().ToList();

            Cards = (
                from s in suits
                from v in values
                select new Card(s, v))
            .ToList();
        }

        public void Shuffle() => Cards = Cards.OrderBy(c => Guid.NewGuid()).ToList();
    }
}
