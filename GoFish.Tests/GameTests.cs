﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFish;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;

namespace GoFish.Tests
{
    [TestClass()]
    public class GameTests
    {
        [TestMethod()]
        public void StartGame_CurrentStatusNotStarted_ChangeStatusToActive()
        {
            // Arrange
            var game = GameWith2Players();
            game.Status = GameStatus.NotStarted;

            // Act
            game.Start();

            // Assert
            Assert.AreEqual(GameStatus.Active, game.Status);
        }

        [TestMethod()]
        public void StartGame_CurrentStatusActive_StatusRemainsActive()
        {
            // Arrange
            var game = GameWith2Players();
            game.Status = GameStatus.Active;

            // Act
            game.Start();

            // Assert
            Assert.AreEqual(GameStatus.Active, game.Status);
        }

        [TestMethod()]
        public void StartGame_CurrentStatusOver_StatusRemainsOver()
        {
            // Arrange
            var game = GameWith2Players();
            game.Status = GameStatus.Over;

            // Act
            game.Start();

            // Assert
            Assert.AreEqual(GameStatus.Over, game.Status);
        }

        [TestMethod()]
        public void AddPlayer_Add1Player_PlayerInPlayersList()
        {
            // Arrange
            var game = GameWith2Players();
            var player = new Player("John");
            var expectedPlayersList = game.Players;
            expectedPlayersList.Add(player);

            // Act
            game.AddPlayer(player);

            // Assert
            expectedPlayersList.Should().BeEquivalentTo(game.Players);
        }

        [TestMethod]
        public void SetDealer_NoExistingDealer_PickFirstPlayer()
        {
            // Arrange
            var expectedDealer = new Player("Player1");
            var game = new Game(expectedDealer, new Player("Player2")); 

            // Act
            game.SetDealer();

            // Assert
            game.Dealer.Should().BeEquivalentTo(expectedDealer);

        }

        [TestMethod]
        public void SetCurrentTurn_NoCurrentPlayer_PickFirstPlayerAfterDealer()
        {
            // Arrange
            var dealer = new Player("Player1");
            var expectedPlayer = new Player("Player2");
            var game = new Game(dealer, expectedPlayer);
            game.SetDealer();

            // Act
            game.SetCurrentTurn();

            // Assert
            game.CurrentTurn.Should().BeEquivalentTo(expectedPlayer);
        }
        
        private Game GameWith2Players()
        {
            var game = new Game(new Player("John"), new Player("Jack"));
            return game;
        }
    }
}