﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoFish;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cards;

namespace GoFish.Tests
{
    [TestClass()]
    public class DealerTests
    {
        [TestMethod]
        public void DealerDeals_CardsDealt_CardsAddedToHands()
        {
            // Arrange
            var deck = new Deck();
            var player1 = new Player("Player1");
            var player2 = new Player("Player2");
            var players = new List<Player> { player1, player2 };
            var numCards = 7;
            var dealer = new Dealer(player1);
        
            // Act
            dealer.Deal(deck, players, numCards);

            // Assert
            Assert.AreEqual(numCards, player1.Hand.Count);
            Assert.AreEqual(numCards, player2.Hand.Count);
        }

        [TestMethod]
        public void DealerDeals_CardsDealt_RemainingCardsReturned()
        {
            // Arrange
            var deck = new Deck();
            var player1 = new Player("Player1");
            var players = new List<Player> { player1 };
            var numCards = 7;
            var dealer = new Dealer(player1);

            // Act
            var returned = dealer.Deal(deck, players, numCards);

            // Assert
            Assert.AreEqual(52 - numCards, returned.Count);
        }
    }
}