﻿using Cards;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace GoFish
{
    public enum GameStatus
    {
        NotStarted,
        Active,
        Over
    }

    public class Game
    {
        private const int CARDS_PER_PLAYER = 7;

        public List<Player> Players { get; } = new List<Player>();
        public Deck Deck { get; set; } = new Deck();
        public Dealer Dealer { get; set; }
        public List<Card> DrawPile { get; set; }
        public Player CurrentTurn { get; set; }
        public GameStatus Status { get; set; } = GameStatus.NotStarted;

        public Game(Player player1, Player player2, params Player[] additionalPlayers)
        {
            Players.Add(player1);
            Players.Add(player2);
 
            foreach (var player in additionalPlayers)
            {
                Players.Add(player);
            }
        }

        public void AddPlayer(Player player)
        {
            Players.Add(player);
        }

        public void Start()
        {
            if (Status == GameStatus.NotStarted)
            {
                Status = GameStatus.Active;
            }
            SetDealer();
            Dealer.Shuffle(Deck);
            DrawPile = Dealer.Deal(Deck, Players, CARDS_PER_PLAYER);
            SetCurrentTurn();
        }

        public void SetDealer()
        {
            Player player = null;
            if (Dealer == null)
            {
                player = Players.FirstOrDefault();
            }
            Dealer = new Dealer(player);
        }

        public void SetCurrentTurn()
        {
            CurrentTurn = Players.Find(p => p.Id != Dealer.Id);
        }
    }
}
