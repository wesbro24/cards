﻿using Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFish
{
    public class Player
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public List<Card> Hand { get; } = new List<Card>();
        public List<Card> Matches { get; }

        public Player(string name)
        {
            Name = name;
        }

        public void AddToHand(Card card)
        {
            Hand.Add(card);
        }
    }
}
