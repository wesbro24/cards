﻿using Cards;
using GoFish;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFish
{
    public class Dealer : Player
    {
        public Dealer(Player player) : base(player.Name)
        {
            base.Id = player.Id;
        }

        public Deck Shuffle(Deck deck)
        {
            deck.Shuffle();
            return deck;
        }

        public List<Card> Deal(Deck deck, List<Player> players, int cardsPerPlayer)
        {
            var cards = deck.Cards;

            foreach (var player in players)
            {
                foreach (var card in cards.Take(cardsPerPlayer))
                {
                    player.AddToHand(card);
                }
                cards.RemoveRange(0, cardsPerPlayer);
            }

            return cards;
        }
    }
}
