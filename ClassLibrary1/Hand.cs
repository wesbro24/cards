﻿using Cards;
using System.Collections.Generic;

namespace GoFish
{
    public class Hand
    {
        public List<Card> Cards { get; set; }
    }
}