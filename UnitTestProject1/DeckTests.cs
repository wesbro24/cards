﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cards;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;

namespace Cards.Tests
{
    [TestClass]
    public class DeckTests
    {
        [TestMethod]
        public void CreateStandardDeck_OnCreate_HasCorrectNumberOfCards()
        {
            // Arrange
            // Act
            var deck = new Cards.Deck();

            // Assert
            Assert.AreEqual(52, deck.Cards.Count);
        }

        [TestMethod]
        public void CreateStandardDeck_OnCreate_HasAllCardsForEachSuit()
        {
            // Arrange
            var suits = Enum.GetValues(typeof(Suit)).Cast<Suit>().ToList();
            var values = Enum.GetValues(typeof(Value)).Cast<Value>().ToList();

            var expectedCards = (
                from s in suits
                from v in values
                select new Card(s, v))
                .ToList();

            // Act
            var deck = new Cards.Deck();

            // Assert
            deck.Cards.Should().BeEquivalentTo(expectedCards);
        }
    }
}
